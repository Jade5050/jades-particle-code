# README #

Particle HTTP Watcher Code

### What is this repository for? ###

Particle IP Watcher ..Watching my Openhab Program

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests 
Turn openhab on and off

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
```
#!C++

#include "application.h"
#include "HttpClient.h"



const int ResetPin =D6 ;
const int AlarmPin =D5 ;
const int OKPin =D4 ;
const int BuzzerPin=D3 ;

unsigned int nextTime = 0;   
HttpClient http;

http_header_t headers[] = {

    { "Accept" , "*/*"},
    { NULL, NULL } 
};

http_request_t request;
http_response_t response;





//
void setup() {
    Serial.begin(9600);
    
pinMode(ResetPin,INPUT);
pinMode(AlarmPin,OUTPUT);
pinMode(OKPin,OUTPUT);
pinMode(BuzzerPin,OUTPUT); 

}

void loop() {
    
    char message[56];

    if (nextTime > millis()) {
        return;
    }

    Serial.println();
    Serial.println("Application>\tStart of Loop.");
    request.hostname = "192.168.1.117";
    request.port = 8080;

    // Get request
    http.get(request, response, headers);
    Serial.print("Application>\tResponse status: ");
    Serial.println(response.status);
//
//    sprintf(message, "%d",response.status);
//    Particle.publish("response.status",message, PRIVATE);
//    delay(100);
    
    if (response.status == -256) {
         digitalWrite(OKPin,HIGH);
         digitalWrite(AlarmPin,LOW);
     }
     else  {
      digitalWrite(AlarmPin,HIGH);
      delay(1000);
      digitalWrite(OKPin,LOW);
      {
      digitalWrite(BuzzerPin,HIGH);
      delay(10000);
      digitalWrite(BuzzerPin,LOW);
      delay(10000);
      }
      
      
    }
//
    Serial.print("Application>\tHTTP Response Body: ");
    Serial.println(response.body);

    nextTime = millis() + 10000;
    
   
    
}

```

* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact